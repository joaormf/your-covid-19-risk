# Your COVID-19 Risk

This is the repository for the Your COVID-19 Risk project, a project uniting over a hundred volunteers with expertise in health promotion and behavior change.

The Open Science Framework repository for this project is located at https://osf.io/vkdyt, and the corresponding DOI is  https://doi.org/10.17605/osf.io/vkdyt.

This project has two Git repositories, hosted on the Open Source platform GitLab. The first repo has the infrastructure, and is located at https://gitlab.com/a-bc/your-covid-19-risk. The second repo has the data pipeline, and is located at https://gitlab.com/a-bc/your-covid-19-risk-data.
The aim of this project is three-fold:

1. Provide a widely usable tool that allows people to get an impression of their COVID-19-related risk, both to themselves and others (through SARS-CoV-2 spreading);
2. Apply behavior change theory to present people with messages that help them decrease their risk;
3. Learn about why our tool users do what regarding their COVID-19-related behaviors so that we can improve the tool and recommend avenues for intervention to governments, policymakers, and prevention and health promotion organisations.

Note that this project is not research; i.e. it is not a study, and so we have no research questions or hypotheses. Instead, this project applies insights from behavior change science to develop a tool to help contain the coronavirus pandemic. This project is engineering instead of science: we are not studying how to build better bridges, we are using what we already know to build a bridge. To properly build this bridge, we need to properly understand the situation. Therefore, to improve this tool, we ask our tool users a number of questions, similar to how smartphone apps ask their users questions, and websites like Amazon ask their users questions. Because the tool users' answers to the questions we ask can also be used by governments and health agencies who want to develop health communications, we make these data public, specifically, by depositing them in the public domain. This means that while researchers are free to use these data for research purposes, the data were not collected for research. As such, we did not preregister this project - preregistration would require hypotheses, or at least research questions or some scientific aim, which this project does not have.

## Background

This project was conceived on Friday the 20th of March 2020 based on 1) the Pandemic Footprint site (https://pandemic-footprint.com), a great tool to help people to limit the spread of the coronavirus; discussions about determinants of COVID-19-related behaviors in the EHPS Corona Talk WhatsApp group; and the RAA DCT project that creates a standard for consistently measuring sub-determinants of a target behavior (https://a-bc.gitlab.io/dct-raa/).

The project was then initiated by the Academy of Behavior Change (https://a-bc.eu), a Dutch non-profit (charity/foundation) contributing to the theory, methodology, application, and knowledge translation of behavior change science, to support prevention organisations to develop effective behavior change interventions, campaigns, and programmes.

## Licensing

We aim to release everything here under permissive licenses. However, we still need to iron out some things (e.g. how to prevent abuse of the logo). Therefore, for now, standard copyright applies to everything in this repository except:

1. R scripts (`.Rmd` files), which we license under CC0, or public domain - do with them what you want;
2. Data sets (`.csv` files), which are anonymized, and as such, defined as facts and existing in the public domain by definition.

We aim to release most materials under licenses comparable to the CC-BY-NC-SA license.

We do ask you, if you use any of these resources, to make your results (manuscripts, presentations, analysis scripts, visualisations, etc) freely available.

## Rendered R Markdown files

The links to the rendered analysis files in this project are:

- https://your-risk.com/v1-dct-specs
- https://your-risk.com/v1-risk-estimation-survey-1-results
- https://your-risk.com/v1-risk-estimation-survey-2-results
- https://your-risk.com/v1-country-prevalences
- https://your-risk.com/v1-translation-results-website
- https://your-risk.com/v1-translation-results-limesurvey
- https://your-risk.com/v1-results

## Theory and technologies

This project uses a number of technologies/methods that facilitate the use of behavior change science in a systematic manner:

1. **The Reasoned Action Approach (RAA)**. The Reasoned Action Approach is a theory that deals with reasoned action (i.e. planned behavior - as its predecessor was called). This theory was published in the 2010 book by Fishbein & Ajzen, Predicting and Changing Behavior. It extensively described how to measure its components. These descriptions have been further specified in a set of Decentralized Construct Taxonomies, which we will use in this project.

2. **Decentralized Construct Taxonomies (DTCs)**. DCTs were developed to enable unequivocal reference to specific constructs with their corresponding definitions and instructions for developing measurement instruments (and code measurement instruments, when doing a literature review; and elicit construct content and code the resulting data, when doing qualitative research). These DCTs enable unequivocal reference to these constructs without requiring central curation. You can read more about them at
https://r-packages.gitlab.io/psyverse/ and the 'Full' tab at https://a-bc.gitlab.io/dct-raa/.  
We will use DCTs to facilitate similar measurement of the relevant (sub-)determinants across languages, by first translating the different question templates as listed at https://a-bc.gitlab.io/dct-raa/ in the 'COVID-19' tab.

3. **Confidence Interval-Based Estimation of Relevance (CIBER) plots**. CIBER plots were developed to efficiently establish which determinants to target in an intervention. They are based on a number of theoretical, methodological, and statistical best practices, as described at https://doi.org/10.3389/fpubh.2017.00165 and https://doi.org/10.31234/osf.io/5wjy4. Also see Chapter 6 in the [Book of Behavior Change](https://bookofbehaviorchange.com/selecting-determinants).  
We will use these to base our recommendations to governments and health agencies on.

4. **Acyclic Behavior Change Diagrams (ABCDs)**. Acyclic Behavior Change Diagrams (ABCDs) are diagrams that illustrate the logic model (also known as ‘theory of change’) underlying any intervention, treatment, or campaign aiming to change some aspect of people’s minds and/or behaviors. Specifically, the ABCD shows the assumed causal and structural assumptions, thereby showing what is assumed to cause what (e.g. which elements of the intervention are assumed to influence which aspects of the target population’s psychology?) and what is assumed to consist of what (e.g. which determinants are assumed to contain which specific aspects of the target population’s psychology?). For more information, see Chapter 9 in the [Book of Behavior Change](https://bookofbehaviorchange.com/abcds) or [this vignette](https://r-packages.gitlab.io/behaviorchange/articles/abcd.html).  
We will use ABCDs to specify the recommendations we provide to people when they completed the risk estimate tool.

5. **LimeSurvey**. This is an excellent Free/Libre Open Source Software package for development of questinnaires. We will use this to program the risk estimation tool, the recommendations people will receive, and ask the questions implementing the DCTs. See https://limesurvey.org for more information.

