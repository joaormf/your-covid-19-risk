---
dct:
  version: 0.1.0
  id: autonomy_conditionPresence_77q1mpmq
  label: "Perceived presence of condition"
  date: "2020-04-24"
  ancestry: "autonomy_conditionPresence_73dnt5zr"       ### The DCT(s) this DCT is based on
  retires:         ### Any DCT(s) that are made obsolete by this one
  source:
    id: "raa_book"

################################################################################

  definition:
    definition: "The perceived presence of a condition refers to the perceived probability that a condition will be present that may have some power to facilitate or obstruct successfully accomplishing the target behavior. Such conditions are perceived to be external to the individuals direct control (though they may in reality be internal conditions; for example, addicts may perceive their craving to be irresistible, and as such, a condition they have no control over and that negatively impacts their autonomy). Facilitating or obstructing factors that are perceived to be internal (i.e. that an individual can learn over time) are subskills (see dct:capacity_subskillPresence_73dnt5zy).

Each perceived presence of a condition concerns the absence or presence of one condition. The perceived nature of the condition (i.e. whether it facilitates or obstructs the target behavior), i.e. the perceived power of that condition, is covered in dct:autonomy_conditionPower_73dnt5zs.

According to the Reasoned Action Approach, perceptions about the presence of conditions combine multiplicatively with perceptions about the power of conditions (see dct:autonomy_conditionPower_73dnt5zs) into autonomy beliefs (see dct:autonomy_belief_73dnt5zt).

Examples of perceived presence of a condition are the perception that to buy condoms, one will have to ask for them at the counter (which may obstruct buying condoms); the perception that it is raining (which may obstruct going for a run); the perception that healthy food is very expensive (which may obstruct buying healthy food); the perception that testing for STIs is free (which may facilitate getting tested for STIs); and the perception that anticonception is widely available (which may facilitate using anticonception)."

    source:
      spec: ""

################################################################################

  measure_dev:
    instruction: "The perceived presence of a condition is measured by asking the perceived probability that a condition will be present. A unidimensional scale is required. The items are formulated as 'How likely do you think it is that [CONDITION]?' with anchors 'Very unlikely' and 'Very likely'.

For example, 'How likely do you think it is that it will rain?' and 'How likely do you think it is that anticonception will be widely available?', both with anchors 'Very unlikely' and 'Very likely'.

In some cases, the condition is not the absence or presence of an environmental condition, but the absence or presence of oneself in a part of one's environment, such as 'being in a busy place', 'being in a supermarket', which are important for behaviors such as keeping one's distance during a global pandemic, 'being in a gay sauna', which is important for behaviors such as condom use during anal intercourse, or 'being in a bar with my friends', which is important for behaviors such as moderating one's alcohol intake. In such cases, instead of measuring the likelihood of a condition existing, it can be better to measure the frequency with which people will find themselves in that situation in the relevant timeframe. For example, 'Over the next seven days, how frequently do you think you will find yourself in a busy place?', with anchors 'never' and 'every day'.

In some exceptional situations, it can be important to avoid specific labeling of the timeframe. In that case, the timeframe can simply be omitted: 'How frequently do you find yourself in a busy place?', with anchors 'never' and 'every day'.

Applying this template to the other examples yields 'How frequently do you find yourself in a bar?' and 'How frequently do you find yourself in a gay sauna?'. Note that the anchors ('never' and 'every day') should be adjusted to both the timeframe and the frequency with which people are present in the relevant environment, such that an optimum is found between representing the full spectrum of people's experience and capturing maximum variation in people's answers. While for most (but not all) environments, 'never' is a reasonable lower anchor, the top anchor can vary; if the timeframe is a year or for uncommon environments, 'every day' may not be a suitable top anchor."

    source:
      spec: ""

################################################################################

  measure_code:
    instruction: ""
    source:
      spec: ""

################################################################################

  manipulate_dev:
    instruction: ""
    source:
      spec: ""

################################################################################

  manipulate_code:
    instruction: ""
    source:
      spec: ""

################################################################################

  aspect_dev:
    instruction: ""
    source:
      spec: ""

################################################################################

  aspect_code:
    instruction: ""
    source:
      spec: ""

################################################################################

  rel:
    id: "autonomy_belief_73dnt5zt"
    type: "causal_influences_product"

################################################################################
---

