This is one of the analysis files in the repository for the Your COVID-19 Risk project, a project uniting over a hundred volunteers with expertise in health promotion and behavior change.

For details, please see the GitLab repository for this project (https://gitlab.com/a-bc/your-covid-19-risk) or the OSF repo (https://osf.io/vkdyt; the DOI for the OSF repo is https://doi.org/10.17605/osf.io/vkdyt).

The links to the rendered analysis files in this project are:

- https://your-risk.com/v1-dct-specs
- https://your-risk.com/v1-risk-estimation-survey-1-results
- https://your-risk.com/v1-risk-estimation-survey-2-results
- https://your-risk.com/v1-country-prevalences
- https://your-risk.com/v1-abcds
- https://your-risk.com/v1-translation-results-website
- https://your-risk.com/v1-translation-results-limesurvey
- https://your-risk.com/v1-volunteer-grid
- https://your-risk.com/v1-results

