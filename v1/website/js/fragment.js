const locationsNavElements = {
  "homepage.html": ["#welcome", "#information", "#volunteers", "#contact"],
  "data.html": ["#homepage", "#data"],
  "disclaimer.html": ["#homepage", "#disclaimer", "#ip", "#cookies"],
  "othertools.html": ["#homepage", "#othertools"],
  "riskmodel.html": ["#homepage", "#riskmodel"],
  "theory.html": ["#homepage", "#theory"],
  "result.html": ["#homepage", "#riskestimate", "#safety", "#share", "#share", "#links"],
  "press.html": ["#homepage", "#press"],
  "allvolunteers.html": ["#homepage", "#volunteers"],
}

history.pushState = ( function(f) {  function pushState(){
  var ret = f.apply(this, arguments);
  window.dispatchEvent(new Event('pushstate'));
  window.dispatchEvent(new Event('locationchange'));
  return ret;
}})(history.pushState);

history.replaceState = ( function(f) { function replaceState(){
  var ret = f.apply(this, arguments);
  window.dispatchEvent(new Event('replacestate'));
  window.dispatchEvent(new Event('locationchange'));
  return ret;
}})(history.replaceState);

window.addEventListener('popstate', function(){
  window.dispatchEvent(new Event('locationchange'))
});

window.addEventListener('hashchange', function(){
  loadFragmentFromDocumentLocation();
})

loadFragmentFromDocumentLocation = function() {
  if( document.location.hash ) {
    let documentHash = document.location.hash;
    if (documentHash.indexOf("?") !== -1) {
      documentHash = documentHash.substring(0, documentHash.indexOf("?"))
    }
    loadFragment(documentHash.substring(1, documentHash.length) + '.html' );
  } else {
    loadFragment('homepage.html');
  }
}

function loadFragment(fragment, navElementsShow ) {
  if ( !navElementsShow ) {
    navElementsShow = locationsNavElements[fragment];
  }
  const navElementsHide = [
    "#welcome",
    "#information",
    "#volunteers",
    "#contact",
    "#homepage",
    "#riskmodel",
    "#theory",
    "#data",
    "#disclaimer",
    "#ip",
    "#cookies",
    "#othertools",
    "#riskestimate",
    "#safety",
    "#share",
    "#links",
    "#press"
  ];

  $.get({
    headers: {"Content-Type": "text/plain; charset=UTF-8"},
    url: "./fragments/"+fragment,
    async: false
  }, function(data, status){
    // set the content and scroll to top
    $("#templateContent")[0].innerHTML = data;

    // show/hide nav bar elements
    navElementsHide.forEach(function(navElement) {
      $('a[href="'+navElement+'"]').hide();
    });
    navElementsShow.forEach(function(navElement) {
      $('a[href="'+navElement+'"]').show();
    });

    // run localize to enable i18 on the fragment
    i18nLocalize();

    window.scrollTo({ top: 0, behavior: 'smooth' });

    //run page specific scripts
    if(fragment === "homepage.html") {
      // volunteers
      window.renderVolunteers(false);

    } else if(fragment == "result.html") {
      document.location.href = document.location.pathname
          + "result.html"
          + document.location.search
          + "&result="
          + document.location.hash.substring(document.location.hash.indexOf("#result?") + "#result?".length, document.location.hash.length);
    } else if(fragment == "allvolunteers.html") {
      // volunteers
      window.renderVolunteers(true);
    }
  });
}
