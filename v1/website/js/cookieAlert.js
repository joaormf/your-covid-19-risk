!function (e) {
  "use strict";

  function isAllowCookie() {
    let cookies = document.cookie.split(';').reduce(function(cookieObject, cookieString) {
      let splitCookie = cookieString.split('=')
      try {
        cookieObject[splitCookie[0].trim()] = decodeURIComponent(splitCookie[1])
      } catch (error) {
        cookieObject[splitCookie[0].trim()] = splitCookie[1]
      }
      return cookieObject
    }, []);
    return cookies["allowCookies"] === "1";
  }

  function setAllowCookie() {
    var d = new Date();
    d.setTime(d.getTime() + (14 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = "allowCookies=1;" + expires + ";path=/";
  }

  if(!isAllowCookie()) {
    $("#cookie-alert").show();
    $(".cookie-alert-close").click(function() {
      setAllowCookie()
      $("#cookie-alert").hide();
    });
  }
}(window);
