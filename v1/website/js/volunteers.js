let volunteers = [];
$.get({
  headers: {"Content-Type": "application/json; charset=UTF-8"},
  url: "./json/volunteers.json",
  async: false
}, function(data, status) {
  if (data) {
    volunteers = data;
  }
});

// https://stackoverflow.com/questions/19269545/how-to-get-n-no-elements-randomly-from-an-array
function getRandom(arr, n) {
  var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
  for(var i=0; i<len; i++){
    arr[i].id=i;
  }
  if (n > len) {
    return arr;
  }
  while (n--) {
    var x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
}

function elementsToRender() {
  const width = Math.max(
      document.documentElement.clientWidth,
      window.innerWidth || 0
  );
  return Math.floor(width/150) * 3;
}

function renderVolunteers(renderAll) {
  const numberVolunteers = renderAll ? volunteers.length : elementsToRender();
  var data = "";
  getRandom(volunteers, numberVolunteers).forEach( function(volunteer) {
    data += "<div class=\"team-member-div\">\n" +
        "      <div class=\"team-member\">\n" +
        "        <div class=\"team-img\">\n" +
        (volunteer.picture
            ? "            <img src=\"img/volunteers/" + volunteer.picture + "\" alt=\""+ volunteer.name +"\" class=\"img-responsive\">\n"
            : "            <i class=\"far fa-user\" class=\"img-responsive\"></i>")+
        "        </div>\n" +
        "        <div class=\"team-country-img\">\n" +
        "          <i class=\"flag-icon flag-icon-"+ volunteer.country +"\" class=\"img-responsive\"></i>"+
        "        </div>\n" +
        "        <div class=\"team-hover\" onclick=\"loadVolunteerModal('"+volunteer.id+"')\">\n" +
        "          <div class=\"desk\">\n" +
        "            <h4>"+ volunteer.name +"</h4>\n" +
        "          </div>\n" +
        "        <div class=\"s-link\">\n" +
        (volunteer.linkedin ? "<a target='_blank' href=\"https://www.linkedin.com/in/"+ volunteer.linkedin +"\"><i class=\"fab fa-linkedin text-palette-secondary\"></i></a>\n" : "") +
        (volunteer.facebook ? "<a target='_blank' href=\"https://www.facebook.com/"+ volunteer.facebook +"\"><i class=\"fab fa-facebook text-palette-secondary\"></i></a>\n" : "") +
        (volunteer.twitter ? "<a target='_blank' href=\"https://twitter.com/"+ volunteer.twitter +"\"><i class=\"fab fa-twitter text-palette-secondary\"></i></a>\n" : "") +
        (volunteer.url ? "<a target='_blank' href=\""+ volunteer.url +"\"><i class=\"fas fa-globe text-palette-secondary\"></i></a>\n" : "") +
        "         </div>\n" +
        "        </div>\n" +
        "      </div>\n" +
        "    </div>";
  });

  $("#volunteersData")[0].innerHTML = data;
}

function loadVolunteerModal(volunteerId) {
  const volunteer = volunteers[volunteerId];

  $("#modal-volunteer-name")[0].innerHTML = "<i class=\"flag-icon flag-icon-"+ volunteer.country +" fa-1x mb-1\" title=\""+volunteer.country.toUpperCase()+"\"></i>&nbsp;"+volunteer.name;
  $("#modal-volunteer-expertise")[0].innerHTML = volunteer.expertise ? volunteer.expertise : "";
  $("#modal-volunteer-contribution")[0].innerHTML = volunteer.contribution ? volunteer.contribution : "";
  $("#modal-volunteer-social-media")[0].innerHTML = (volunteer.linkedin ? "<a target='_blank' href=\"https://www.linkedin.com/in/"+ volunteer.linkedin +"\"><i class=\"fa-2x mb-2 fab fa-linkedin\"></i></a>&nbsp;" : "") +
    (volunteer.facebook ? "<a target='_blank' href=\"https://www.facebook.com/"+ volunteer.facebook +"\"><i class=\"fa-2x mb-2 fab fa-facebook\"></i></a>&nbsp;" : "") +
    (volunteer.twitter ? "<a target='_blank' href=\"https://twitter.com/"+ volunteer.twitter +"\"><i class=\"fa-2x mb-2 fab fa-twitter\"></i></a>&nbsp;" : "") +
    (volunteer.url ? "<a target='_blank' href=\""+ volunteer.url +"\"><i class=\"fa-2x mb-2 fas fa-globe\"></i></a>" : "");

  $("#volunteerModal").modal()
}
