function assembleGauge(gaugeValue) {
  // https://bernii.github.io/gauge.js/

  var opts = {
    angle: 0, // The span of the gauge arc
    lineWidth: 0.3, // The line thickness
    radiusScale: 1, // Relative radius
    pointer: {
      length: 0.6, // // Relative to gauge radius
      strokeWidth: 0.035, // The thickness
      color: '#000000' // Fill color
    },
    limitMax: true,     // If false, max value increases automatically if value > maxValue
    limitMin: true,     // If true, the min value of the gauge will be fixed
    strokeColor: '#E0E0E0',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support
    staticZones: [
      {strokeStyle: "#F03E3E", min: 0, max: 10},
      {strokeStyle: "#FFDD00", min: 10, max: 30},
      {strokeStyle: "#30B32D", min: 30, max: 70},
      {strokeStyle: "#FFDD00", min: 70, max: 90},
      {strokeStyle: "#F03E3E", min: 90, max: 100}
    ]
  };
  var target = document.getElementById("resultsGaugeCanvas"); // canvas element
  var gauge = new Gauge(target).setOptions(opts);
  gauge.maxValue = 100; // set max gauge value
  gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
  gauge.animationSpeed = 64; // set animation speed (32 is default value)
  gauge.set(gaugeValue); // set actual value
  //gauge.setTextField(document.getElementById("gaugeValue"));
}

function renderResults() {
  // http://localhost:63342/website/result.html?lang=fr&result=443930111100122xx00

  let queryString = parseQuery(document.location.href)["result"];

  const gettingOnImage = queryString.substring(0, 1);
  const gettingOffImage = queryString.substring(1, 2);
  const gaugeValue = queryString.substring(2, 4);
  const proximityMessageIdx = queryString.substring(4, 5);
  const selfIsolationMessages = [];
  const selfIsolationStartPos = 6;
  const numberSelfIsolationMessages = 8;
  selfIsolationMessages.push(queryString.substring(5, 6) == "1");
  selfIsolationMessages.push(queryString.substring(6, 7) == "1");
  selfIsolationMessages.push(queryString.substring(7, 8) == "1");
  selfIsolationMessages.push(queryString.substring(8, 9) == "1");
  selfIsolationMessages.push(queryString.substring(9, 10) == "1");
  selfIsolationMessages.push(queryString.substring(10, 11) == "1");
  selfIsolationMessages.push(queryString.substring(11, 12) == "1");
  selfIsolationMessages.push(queryString.substring(12, 13) == "1");
  const handWashMessage1Idx = queryString.substring(13, 14);
  const handWashMessage2Idx = queryString.substring(14, 15);
  const messagesCountryRegionCode = queryString.substring(15, 19);

  // LANGUAGE
  //i18next.changeLanguage(lang);
  //$('.selectpicker').selectpicker("val", lang);

  // GETTING ON/OFF
  $("#gettingOnDivPlaceholder")[0].innerHTML = "<img src=\"img/result/v1GettingitOnYou_" + gettingOnImage + ".png\" class=\"img-fluid\" width=\"260px\"/>";
  $("#gettingOffDivPlaceholder")[0].innerHTML = "<img src=\"img/result/v1GettingRid_" + gettingOffImage + ".png\" class=\"img-fluid\" width=\"260px\"/>";

  // GAUGE
  assembleGauge(gaugeValue);

  // load json files
  const constFiles = ["distRec.json", "distRef.json", "commonCloseContactGreeting.json",
					  "symptomAction.json", "listSituationsHandWashingRec.json",
					  "resURLsHwExmpls.json", "resURLsTempMnem.json", "resURLsStHmTips.json", "resURLsStayHmIn.json"];
  // const jsonFiles = {};
  const externalResourcesKeys = [];
  const i18nInterpolationValues = {};
  constFiles.forEach(function (file) {
    $.get({
      headers: {"Content-Type": "application/json; charset=UTF-8"},
      url: "./json/" + file,
      async: false
    }, function (data) {
      //Object.assign(jsonFiles, data); IE 11 doesn't support this
      if (data != null) {
        Object.keys(data).forEach(function (key) {
          const interpolationKey = file.substring(0, file.indexOf("."));
          if ( !i18nInterpolationValues[interpolationKey] && messagesCountryRegionCode.match(data[key].regex) != null) {
            externalResourcesKeys.push(data[key].key);
            i18nInterpolationValues[interpolationKey] = "$t(" + data[key].key + ")";//i18next.t(data[key].key);
          }
        });
      }
      //$.extend(jsonFiles, data)
    });
  });

  // LINKS
  for (let i = 5; i < externalResourcesKeys.length; i++) {
    const key = externalResourcesKeys[i];
    const valueToRender = i18next.t(key);
    if (valueToRender && valueToRender !== "") {
      $("#linkText" + (i + 1)).attr( "data-i18n", "[html]"+key);
    } else {
      $("#linkText" + (i + 1)).hide();
    }
  }

  const escapedJsonData = JSON.stringify(i18nInterpolationValues).split("'").join("\'");

  // PROXIMITY
  for (let i = 1; i < 100; i++) { // 100 because why not... we expect for to break
    const i18nKey = "px_msg1_p5v" + proximityMessageIdx + "_text" + i;
    if (i18next.exists(i18nKey)) {
      $("#proximityText").append("<p data-i18n-options='" + escapedJsonData + "' data-i18n=\"[html]" + i18nKey + "\"/>")
    } else {
      break;
    }
  }

  // SELF ISOLATION
  for (let msgNum = 1; msgNum <= numberSelfIsolationMessages; msgNum++) {
    for (let siNum = 0; siNum < numberSelfIsolationMessages; siNum++) {
      for (let i = 1; i < 100; i++) { // 100 because why not... we expect for to break
        if (selfIsolationMessages[siNum]) {
          const i18nKey = "si_msg" + msgNum + "_p" + (siNum + selfIsolationStartPos) + "v1_text" + i;
          if (i18next.exists(i18nKey)) {
            $("#isolation").append("<p data-i18n-options='" + escapedJsonData + "' data-i18n=\"[html]" + i18nKey + "\"/>")
          } else {
            break;
          }
        } else {
          break;
        }
      }
    }
  }

  // HAND WASH (handWashText1)
  for (let i = 1; i < 100; i++) { // 100 because why not... we expect for to break
    const i18nKey = "hw_msg1_p14v" + handWashMessage1Idx + "_text" + i;
    if (i18next.exists(i18nKey)) {
      $("#handWashText1").append("<p data-i18n-options='" + escapedJsonData + "' data-i18n=\"[html]" + i18nKey + "\"/>")
    } else {
      break;
    }
  }
  for (let i = 1; i < 100; i++) { // 100 because why not... we expect for to break
    const i18nKey = "hw_msg2_p15v" + handWashMessage2Idx + "_text" + i;
    if (i18next.exists(i18nKey)) {
      $("#handWashText2").append("<p data-i18n-options='" + escapedJsonData + "' data-i18n=\"[html]" + i18nKey + "\"/>")
    } else {
      break;
    }
  }

  // LINKS
  const encodedUrl = encodeURIComponent(document.location.href);
  //const encodedUrl = encodeURIComponent(document.location.origin+"/fragments/result.html?"+document.location.search);
  if( mobileAndTabletCheck() ) {
    $("#mobileShareLinks").show();
    $("#shareWhatsApp").attr("href", "whatsapp://send?text="+encodedUrl);
    $("#shareFBMessenger").attr("href", "fb-messenger://share/?link="+encodedUrl);
    $("#shareQQChat").attr("href", "http://service.weibo.com/share/share.php?url="+encodedUrl+"&title="+i18next.t("webpage_title")+"&language="+i18next.language);
    //$("#shareWeChat").attr("href", "");
    //$("#shareSnapchat").attr("href", "");
    $("#shareViber").attr("href", "viber://forward?text="+encodedUrl);
    $("#shareTelegram").attr("href", "https://telegram.me/share/url?url="+encodedUrl+"&text=" + i18next.t("webpage_title"));
  }
  // $("#shareInstagram").attr("href", "");
  $("#shareTwitter").attr("href", "https://twitter.com/intent/tweet?url="+encodedUrl);
  $("#shareLinkedin").attr("href", "https://www.linkedin.com/sharing/share-offsite/?url="+encodedUrl+"&title="+ i18next.t("webpage_title"));
  $("#shareFacebook").attr("href", "https://www.facebook.com/dialog/share?app_id=539537156708264&display=popup&href="+encodedUrl+"&quote="+ i18next.t("webpage_title"));

  // process i18n to translate all the messages
  doi18n();
}

function parseQuery(queryString) {
  var query = {};
  var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
  for (var i = 0; i < pairs.length; i++) {
    var pair = pairs[i].split('=');
    query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
  }
  return query;
}

function mobileAndTabletCheck() {
  let check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};
